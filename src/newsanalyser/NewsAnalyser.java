/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newsanalyser;

import javax.swing.UIManager;

/**
 *
 * @author timro
 */
public class NewsAnalyser {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // TODO code application logic here
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {
            //Do nothing
        }
        MainWindow mw = new MainWindow();
        mw.setVisible(true);
    }
    
}
