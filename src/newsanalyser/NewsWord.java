/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newsanalyser;

/**
 *
 * @author timro
 */
public class NewsWord {
    private String word;
    private int count;
    
    public NewsWord(String word) {
        this.word = word;
        count = 1;
    }
    
    public void addOccurance() {
        count++;
    }
    
}
